# Conscious Living

This repository holds a carefully structured collection of mental models, rules, and tools for living with conscious presence and compassion.

## Sources and inspiration

These Conscious Living (CL) principles are largely adapted from the structure of "commitments" of Conscious Leadership as organized by the [Conscious Leadership Group](https://conscious.is) which is led by Jim Dethmer and Diana Chapman. They collected ideas from numerous specific sources and synthesized them into a holistic structure with precise language. Several of their "commitments" are specific practices from wisdom teachers including Byron Katie, the Sedona Method, and the Hendricks Institute. Other categories draw on a mix of generations-old traditions such as Buddhism and Stoicism.

Besides the value of curating the world's best wisdom practices and ethical mandates, major value comes from having a particularly clear, concise structure. Framing the ideas in a simple and memorable way makes it easier to remember the ideas right when we feel most reactive and are most in need of the wisdom.

## Language and structure

The "Leadership" label works well to reach corporate executives (with their high-budgets to pay for coaching), but nothing in any of these ideas is specific to leadership unless we stretch "leadership" (as CLG does) to apply to everyone in any role and to everything in life *including* being a good follower.

We have switched to the term "living" to better express the wide application of these practices. We suspect that using "living" will also reduce the resistence that some people have to the "leadership" language that reminds them of superficial corporate marketing. Of course, there's also a risk that others get turned off by language that they take as too "woo woo" mystical or similar. We aim for language that will successfully reach the widest audience, but we accept the impossibility of perfection.

We have also restructured the ideas into a tree-style hierarchy, adjusted several of the core concepts and wording, added missing principles, and incorporated many additional insights from other sources.

## Aspiration for CL

We aspire to have the CL principles and structure expressed well enough that all life situations and advice from any source can easily fit the basic CL model. We will adjust the model as necessary toward this goal.

## Project status

August 2022: We are slowly building the repository as we process notes and decide on the best core structure for everything.

## Authors

Aside from the files marked with attribution to The Conscious Leadership Group, the rest of the files here were authored directly by Aaron Wolf with help from Praveen Venkataramana.

## Sources

The CL ideas come from so many sources, and the path by which they got to this collection does not indicate anything about originality. As with ideas in all topics, everything builds upon prior influences. Eventually, we hope to describe at least some version of a list of acknowledgements of sources. Initially, we are prioritizing getting the CL principles organized for us and everyone else to put to use. We don't currently have a whole-body-yes to prioritizing the work of making a thorough list of sources and references. 
