# Conscious Living copyright licensing

All files that include attribution to "The Conscious Leadership Group" are under the terms of the [CLG Content Use Policy](CLG-license.md).

All other files are under the terms of [CC BY-SA](CC-BY-SA.md), the Creative Commons Attribution Share-Alike International License v4.0.

## FLO principles

FLO stands for Free/Libre/Open. FLO includes many facets and implications. The core principles include an attitude celebrating sharing, collaboration, abundance, freedom, and creativity. This stands in contrast to exclusionary "All Rights Reserved" limitations that copyright law uses by default. Instead, a FLO copyright license grants the general public the *freedoms* to access, use, study, modify, and share.

In the case of wisdom like Conscious Living ideas, all language has flaws. Language is an abstraction which different people interpret in different ways at different times. We want collaboration and consensus to the degree that it helps craft and express the ideas and most effectively reach the most people. Everyone also needs the freedom to adapt the ideas to work best for themselves and their particular communities and cultures. We celebrate translation and adaptation, and we invite creative insights to be shared whenever feasible in order to help others and to continually improve and update the main version. All of the CL ideas come from synthesizing and adapting all sorts of sources from all sorts of places and people and times across the world.

## CC BY-SA summary

All CC licenses permit access and sharing. The "BY" refers to **attribution**, retaining the author's name (as identified on the work or noted in instructions where published) and a reference URL for accessing the original work. The "SA" refers to **share-alike**, meaning that any redistribution or adaptations (clearly marked as modified in that case) of the work must also use and indicate the same CC BY-SA license, passing on the freedoms to others.

## CLG policy summary

The CLG policy is a commercial-rights-reserved set of terms that is written more from a human-centered conscious perspective than from a legal one. Still, the policy has clear legal ramifications. It explicitly grants permission for use of materials as long as the following two requirements are met:

- All uses must include attribution. 
    - When mostly unchanged, mark "Created by The Conscious Leadership Group"
    - For altered works, mark "Adapted from The Conscious Leadership Group"
    - Mark clear attribution for all sources when combining works from multiple sources
- Selling of CLG material is prohibited
    - This does *not* block all commercial use, it only blocks sales of the content itself. Using the material within a business is permitted.

The CLG policy also includes a *request* for voluntary donations in gratitude for the CLG work and their open sharing of it.

### CLG policy commentary

We imagine that the CLG team would more fully embrace FLO concepts if only they understood them better. They clearly have their hearts in a place of sharing and abundance, which is the core of FLO principles. They simply have yet to fully process and consider the ramifications of sharing and abundance and the importance of a fully-FLO license like CC BY-SA. Note that the CLG policy uses the term "contribute" to mean financial donations. They have not set up the policy nor the manner of publishing of their works to support *creative* contributions. They would likely embrace collaborative creative input if they had a practical way to manage it and experienced the benefits.

The primary problem with the CLG Policy is its incompatibility with the main similar license, CC BY-SA. Because of the incompatibility, content under each license needs to be carefully managed and kept distinct, and that hampers free creativity.

A nitpick with the wording of the CLG Policy: it states that all audio and video should use the unadapted attribution, which implies that adapting audio and video is either not allowed or at least is not expected. However, this is later contradicted by their branding section which says "…if you use [videos] in their entirety", implying that they *may* be edited.

The main limitation in the CLG policy that is not in CC BY-SA is the prohibition on sales, and that actually makes sense. They don't want others to abuse their generosity by paywalling content that is available freely. We support this inclination. The CLG Policy is superior to the (non-FLO) NC (Non-Commercial) clause in some CC licenses because the NC term blocks *all* commercial use, while the CLG policy only blocks paywalling. The ideal ethical approach is to keep everything free for all to access while allowing reasonable situations like the inclusion of some content in a book which people are allowed to pay for printed copies *as long as* the book is also available online freely. No license does this perfectly. Blocking sales of free material is understandable, and we celebrate the CLG openness to other commercial use. After all, the ideas should be used in businesses everywhere, and we don't want any obstacles and restrictions to that.

The sales-prohibition does *not* block the one other potential commercial abuse: using the content to get attention and selling ads on the side. This brings up a conundrum. Blocking the use of the material alongside ads would block it from being shared on ad-based websites like blogs and social media. There are advantages to disseminating the ideas in whatever places people will see them most. We can wish that ad-based publishing would disappear entirely, but while it remains, maybe it is good that policies and licenses do not block the sharing of content in those contexts. Ideally, the sharing still *emphasizes* that the content is available elsewhere, encouraging people to leave the ad-based platforms and access the content directly where it is free and unencumbered. Overall, both the CLG Policy and CC BY-SA keep a good balance by focusing on attribution without blocking dissemination. We can work to reduce the ills of advertising through means other than through copyright licenses.
